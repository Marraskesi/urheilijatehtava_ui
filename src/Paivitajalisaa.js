import React from 'react';
import {Button, Form} from "react-bootstrap";

const Paivitajalisaa = ({henkilo, setHenkilo, tallenna}) => {
    return (
        <Form onSubmit={tallenna}>
            <Form.Group className="mb-3" controlId="joku">
                <Form.Label>Etunimi</Form.Label>
                <Form.Control type="text" placeholder="Syötä etunimi" defaultValue={henkilo.etunimi}
                              onChange={(e) => setHenkilo("etunimi", e.target.value)}/>
            </Form.Group>
            <Form.Group className="mb-3" controlId="joku2">
                <Form.Label>Sukunimi</Form.Label>
                <Form.Control type="text" placeholder="Syötä sukunimi" defaultValue={henkilo.sukunimi}
                              onChange={(e) => setHenkilo("sukunimi", e.target.value)}/>
            </Form.Group>
            <Form.Group className="mb-3" controlId="joku8">
                <Form.Label>Kutsumanimi</Form.Label>
                <Form.Control type="text" placeholder="Syötä kutsumanimi" defaultValue={henkilo.kutsumanimi}
                              onChange={(e) => setHenkilo("kutsumanimi", e.target.value)}/>
            </Form.Group>
            <Form.Group className="mb-3" controlId="joku3">
                <Form.Label>Syntymävuosi</Form.Label>
                <Form.Control type="number" placeholder="Syötä syntymävuosi" min={1900} max={2022} defaultValue={henkilo.syntymavuosi}
                              onChange={(e) => setHenkilo("syntymavuosi", e.target.value)}/>
            </Form.Group>
            <Form.Group className="mb-3" controlId="joku4">
                <Form.Label>Paino</Form.Label>
                <Form.Control type="number" placeholder="Syötä paino" defaultValue={henkilo.paino}
                              onChange={(e) => setHenkilo("paino", e.target.value)}/>
            </Form.Group>
            <Form.Group className="mb-3" controlId="joku5">
                <Form.Label>Kuva</Form.Label>
                <Form.Control type="url" placeholder="Syötä linkki kuvaan" defaultValue={henkilo.linkki}
                              onChange={(e) => setHenkilo("linkki", e.target.value)}/>
            </Form.Group>
            <Form.Group className="mb-3" controlId="joku6">
                <Form.Label>Laji</Form.Label>
                <Form.Control type="text" placeholder="Syötä urheilulaji" defaultValue={henkilo.laji}
                              onChange={(e) => setHenkilo("laji", e.target.value)}/>
            </Form.Group>
            <Form.Group className="mb-3" controlId="joku7">
                <Form.Label>Saavutukset</Form.Label>
                <Form.Control type="text" placeholder="Syötä saavutukset" defaultValue={henkilo.saavutukset}
                              onChange={(e) => setHenkilo("saavutukset", e.target.value)}/>
            </Form.Group>
            <Button type="submit">Tallenna</Button>
        </Form>
    );
};

export default Paivitajalisaa;