import React from 'react';
import {Button, Container} from "react-bootstrap";
import {Pencil, Trash} from "react-bootstrap-icons";
import axios from "axios";

const Urheilijarivi = ({urheilija, paivitaurheilija}) => {

    function poistaurheilija(urheilijaid) {
        axios.delete(`http://localhost:3000/urheilijat/${urheilijaid}`).then(response => {
            console.log(response);
        })
            .catch(error => {
            console.log(error);
        });
    }



  return (
      <tr>
          <td>{urheilija.etunimi}</td>
        <td>{urheilija.sukunimi}</td>
        <td>{urheilija.kutsumanimi}</td>
        <td>{urheilija.syntymavuosi}</td>
        <td>{urheilija.paino}</td>
        <td><a href={urheilija.linkki}>valokuva</a></td>
        <td>{urheilija.laji}</td>
        <td>{urheilija.saavutukset}</td>
          <td>
              <Container>
                <Button variant="success" onClick={(e)=>paivitaurheilija(urheilija)}><Pencil/></Button>
                <Button variant="danger" onClick={(e)=>poistaurheilija(urheilija.id)}><Trash/></Button>
              </Container>
          </td>
      </tr>
  );
};


export default Urheilijarivi;