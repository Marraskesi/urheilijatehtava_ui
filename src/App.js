import './App.css';
import {useState} from "react";
import axios from "axios";
import Urheilijarivi from "./Urheilijarivi";
import Paivitajalisaa from "./Paivitajalisaa";
import {Button, Container, Stack, Table} from "react-bootstrap";

function App() {
    const [urheilijat, setUrheilijat] = useState([]);
    const [naytalomake, setNaytalomake] = useState(false);
    const [isLoading, setIsLoading] = useState(false);
    const [henkilo, setHenkilo] = useState({
        "etunimi": "",
        "sukunimi": "",
        "kutsumanimi" : "",
        "syntymavuosi": "",
        "paino": "",
        "linkki": "",
        "laji": "",
        "saavutukset": ""
    });

    const tyhja_henkilo = {
        "etunimi": "",
        "sukunimi": "",
        "kutsumanimi" : "",
        "syntymavuosi": "",
        "paino": "",
        "linkki": "",
        "laji": "",
        "saavutukset": ""
    };

    function henk(k, v) {
        setHenkilo({...henkilo, [k]: v});
    }

    function haeurheilijat() {
        setIsLoading(true);
        axios.get("http://localhost:3000/urheilijat")
            .then(response => {
                console.log(response.data);
                setUrheilijat(response.data);
                setIsLoading(false);
            });
    }

    function lisaaurheilija() {
        setHenkilo(tyhja_henkilo);
        setNaytalomake(true);

    }

    function paivitaurheilija(urheilija) {
        setHenkilo(urheilija);
        setNaytalomake(true);
    }

    function tallenna(e) {
        const form = e.currentTarget;
        e.preventDefault();
        e.stopPropagation();
        if (form.checkValidity() === false) {

        } else {
            console.log(henkilo);
            if (henkilo.id) {
                axios.put(`http://localhost:3000/urheilijat/${henkilo.id}`, henkilo)
                    .then(response => {
                        console.log(response.data);
                        haeurheilijat();
                        setNaytalomake(false);
                    });
            } else {
                axios.post("http://localhost:3000/urheilijat", henkilo)
                    .then(response => {
                        console.log(response.data);
                        haeurheilijat();
                        setNaytalomake(false);
                    });
            }

        }

                };

    return (
        <Container>
            <Stack gap={3} className="pt-3">
                <div className="bg-light border">
                    <Stack gap={2}>
                        <div className="bg-light">
                            <Button
                                variant="primary"
                                disabled={isLoading}
                                onClick={!isLoading ? haeurheilijat : null}
                            >
                                {isLoading ? "Ladataan..." : "Hae urheilijat"}
                            </Button>
                        </div>
                        <div className="bg-light">
                            <Table striped bordered hover>
                                <tbody>
                                {urheilijat.map(urheilija => <Urheilijarivi key={urheilija.id} urheilija={urheilija} paivitaurheilija={paivitaurheilija}/>)}
                                </tbody>
                            </Table>
                        </div>
                    </Stack>
                </div>
                <div className="bg-light border">
                    <Stack gap={2}>
                        <div className="bg-light">
                            <Button
                                variant="primary"
                                disabled={isLoading}
                                onClick={!isLoading ? lisaaurheilija : null}
                            >
                                {isLoading ? "Ladataan..." : "Lisää urheilija"}
                            </Button>
                            {naytalomake ?
                                <Paivitajalisaa setHenkilo={henk} henkilo={henkilo} tallenna={tallenna}/> : null}
                        </div>
                    </Stack>
                </div>
            </Stack>
        </Container>

    );
}

export default App;
